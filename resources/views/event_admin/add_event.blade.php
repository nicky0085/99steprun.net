<!doctype html>
 <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>99STEPRUN</title>
   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png'">
    <link rel="shortcut icon" href="'favicon.ico')>

    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.c') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/scss/style.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
               <!--  <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
            </div>

           <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.html"> <i class="menu-icon fa fa-dashboard"></i>99STEPRUN</a>
                    </li>
                    <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Admin</a>


                     <li class="menu-item-has-children dropdown">
                       <a href="{{ route('event_view') }}"><i class="menu-icon fa fa-laptop"></i>Event</a>

                    </li>
                     <li class="menu-item-has-children dropdown">
                         <a href="event.html" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>BIB </a>

                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="summary_bib.html"> Summary BIB </a></li> 
                        </ul>
                        


                       
                    </li>

                    <h3 class="menu-title">Icons</h3><!-- /.menu-title -->

                    
                   
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                        <div class="dropdown for-notification">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="count bg-danger">5</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">You have 3 Notification</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Server #1 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Server #2 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Server #3 overloaded.</p>
                            </a>
                          </div>
                        </div>

                        <div class="dropdown for-message">
                          <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="message"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-email"></i>
                            <span class="count bg-primary">9</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="message">
                            <p class="red">You have 4 Mails</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-3" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                                <a class="nav-link" href="#"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                      
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <strong class="card-title">CREATE EVENT </strong>
                        </div>

                        <div class="col-md-12"> 
                            <div class="text-right">
                                <a href="">
                                <button type="button" class="btn btn-info btn-sm">  BACK </button>
                                <p>
                                </a>
                            </div>   
                       </div>

                       <div class="card-body card-block">

                <!-- <form  method="post" action="{{ route('save') }}" enctype="" class="form-horizontal"> -->
                 <form method="POST" action="{{ route('save') }}" aria-label="{{ __('save') }}" id="">
                        @csrf

                          <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">EVENTNAME</label></div>
                              <div class="col-12 col-md-9">
                                <input type="text" id="text-input" name="eventname" placeholder="Text" class="form-control"><small class="form-text text-muted"></small>
                            </div>
                          </div>

                        <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">DESCRIPTION</label></div>
                            <div class="col-12 col-md-9"><textarea name="description" id="textarea-input" rows="9" placeholder="Content..." class="form-control"></textarea></div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">BANKING</label></div>
                            <div class="col-12 col-md-9"><textarea name="bank" id="bank" rows="4" placeholder="Content..." class="form-control"></textarea></div>
                        </div>


                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">EVEN TYPE</label>
                            </div>

                            <div class="col col-md-9">
                              <div class="form-check">

                                <div class="checkbox">
                                  <label for="checkbox1" class="form-check-label ">
                                    <input type="checkbox" id="funrun" name="funrun" value="FUNRUN" class="form-check-input"> FUNRUN
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox2" class="form-check-label ">
                                    <input type="checkbox" id="mini" name="mini" value="MINI MARATHON" class="form-check-input"> MINI MARATHON
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="half" name="half" value="HALF MARATHON
                                    " class="form-check-input"> HALF MARATHON
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="vip" name="vip" value="VIP" class="form-check-input"> VIP
                                  </label>
                                </div>

                                <div class="checkbox">
                                   <label for="checkbox3" class="form-check-label ">
                                    <input type="text-input"  name="other1" value="" class="" placeholder="OTHER"> 
                                  </label>
                                </div>
                                <p>
                                 <div class="checkbox">
                                   <label for="checkbox3" class="form-check-label ">
                                    <input type="text-input"  name="other2" value="" class="" placeholder="OTHER"> 
                                  </label>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class=" form-control-label">GENDER </label>
                                </div>
                                <div class="col col-md-9">

                                  <div class="form-check-inline form-check">
                                    <label for="inline-radio1" class="form-check-label ">
                                      <input type="radio" id="M" name="GENDER" value="ชาย" class="form-check-input"> ชาย 
                                    </label>

                                    <label for="inline-radio2" class="form-check-label ">
                                      <input type="radio" id="F" name="GENDER" value="หญิง" class="form-check-input"> หญิง
                                    </label>

                                    <label for="inline-radio3" class="form-check-label ">
                                      <input type="radio" id="MF" name="GENDER" value="ชายหญิง" class="form-check-input"> ชายหญิง
                                    </label>
                                  </div>
                                </div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">SECTION AGE</label>
                            </div>

                            <div class="col col-md-9">
                              <div class="form-check">

                                <div class="checkbox">
                                  <label for="checkbox1" class="form-check-label ">
                                    <input type="checkbox" id="age_1" name="age_1" value="ไม่แบ่งรุ่นอายุ" class="form-check-input"> ไม่แบ่งรุ่นอายุ
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox2" class="form-check-label ">
                                    <input type="checkbox" id="age_2" name="age_2" value="ไม่เกิน 20 ปี" class="form-check-input"> ไม่เกิน 20 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_3" name="age_3" value="20-29 ปี" class="form-check-input"> 20-29 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_4" name="age_4" value="30-39 ปี" class="form-check-input"> 30-39 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_5" name="age_5" value="40-49 ปี" class="form-check-input"> 40-49 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_6" name="age_6" value="50-59 ปี" class="form-check-input"> 50-59 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_7" name="age_7" value="50 ปี ขึ้นไป" class="form-check-input"> 50 ปี ขึ้นไป
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_8" name="age_8" value=" 60 ปี  ขึ้นไป" class="form-check-input"> 60 ปี  ขึ้นไป
                                  </label>
                                </div>

                              
                                <p>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_9" name="age_9" value="20-25 ปี" class="form-check-input"> 20-25 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_10" name="age_10" value="26-29 ปี" class="form-check-input"> 26-29 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_11" name="age_11" value="30-35 ปี" class="form-check-input"> 30-35 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_12" name="age_12" value="36-39 ปี" class="form-check-input"> 36-39 ปี
                                  </label>
                                </div>

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_13" name="age_13" value="40-45 ปี" class="form-check-input"> 40-45 ปี
                                  </label>
                                </div>

                                 <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_14" name="age_14" value="46-49 ปี" class="form-check-input"> 46-49 ปี
                                  </label>
                                </div>

                                 <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_15" name="age_15" value="50-55 ปี" class="form-check-input"> 50-55 ปี
                                  </label>
                                </div>

                                 <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="age_16" name="age_16" value=" 56-59 ปี" class="form-check-input"> 56-59 ปี
                                  </label>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div class="row for-group">
                            <div class="col col-md-3">
                                <label class="form-control-label">SIZE SHIRT</label>
                            </div>
                            <div class="col col-md-9">

                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="ss34" name="ss34" value="SS 34 นิ้ว" class="form-check-input"> SS 34 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="s36" name="s36" value=" 36 นิ้ว" class="form-check-input"> S 36 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="m38" name="m38" value="M 38 นิ้ว" class="form-check-input"> M 38 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="l40" name="l40" value="L 40 นิ้ว" class="form-check-input"> L 40 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="xl42" name="size42" value="XL 42 น้ิว" class="form-check-input"> XL 42 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="2xl44" name="size44" value="2 XL 44 นิ้ว" class="form-check-input"> 2 XL 44 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="3xl46" name="size46" value="3 XL 46 นิ้ว" class="form-check-input"> 3 XL 46 นิ้ว
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label for="checkbox3" class="form-check-label ">
                                    <input type="checkbox" id="4xl48" name="size48" value="4 XL 48 นิ้ว" class="form-check-input"> 4 XL 48 นิ้ว
                                  </label>
                                </div>
                            </div>  

                                         
                        </div>
                      

                        <div class="card-footer text-center" >

                             <button type="submit" class="btn btn-info btn-sm"" value="Save">
                                   <i class="fa fa-dot-circle-o"></i>  Save 
                            </button>
                            
                            <button type="reset" class="btn btn-danger btn-sm">
                              <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
                      </form>

                      </div>
                    </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


 

    <script src="{{ asset('assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js')}}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>


    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/datatables-init.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable({
            "scrollX":'1280px',

            });
        });

        $('button[name="btnViewInfo"]').click(function(){
            location.href = ( '{{ url("Admin/view_admin") }}/'+$(this).data('id') );
        });

    </script>




     
        

</body>
</html>
