<!doctype html>
 <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>99STEPRUN</title>
   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png'">
    <link rel="shortcut icon" href="'favicon.ico')>

    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.c') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/scss/style.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
               <!--  <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
            </div>

           <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.html"> <i class="menu-icon fa fa-dashboard"></i>99STEPRUN</a>
                    </li>
                    <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Admin</a>


                     <li class="menu-item-has-children dropdown">
                       <a href="{{ route('event_view') }}"><i class="menu-icon fa fa-laptop"></i>Event</a>


                       
                    </li>
                     <li class="menu-item-has-children dropdown">
                         <a href="event.html" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>BIB </a>

                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="summary_bib.html"> Summary BIB </a></li> 
                        </ul>
                        


                       
                    </li>

                    <h3 class="menu-title">Icons</h3><!-- /.menu-title -->

                    
                   
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                        <div class="dropdown for-notification">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="count bg-danger">5</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">You have 3 Notification</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Server #1 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Server #2 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Server #3 overloaded.</p>
                            </a>
                          </div>
                        </div>

                        <div class="dropdown for-message">
                          <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="message"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-email"></i>
                            <span class="count bg-primary">9</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="message">
                            <p class="red">You have 4 Mails</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-3" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                                <a class="nav-link" href="#"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                      
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <strong class="card-title">EVENT MANAGEMENT</strong>
                        </div>

                        <div class="col-md-12">
                            <br>
                            <div class="text-right">
                                <a href="{{ route('event_view') }}">
                                <button type="button" class="btn btn-danger btn-sm"> BACK </button>
                                <p>
                                </a>
                            </div>   
                       </div>
                    
                  <table id="bootstrap-data-table" class="table table-hover table-striped table-align-middle mb-0" size="sm">
                            <tbody>
                            @if (isset($event_id))
                                @php
                                  $i=1;
                                @endphp
                                @foreach ($event_id as $key => $row)

                            <tr class="text-center">
                                <td>id</td>
                                <td>{{ $i }}</td>
                            </tr>
                            <tr class="text-center">
                                <td>eventname : </td>
                                <td>{{ $row->name_evt }}</td>
                            </tr>

                             <tr class="text-center">
                                <td>description : </td>
                                <td>{{ $row->description }}  </td>
                            </tr> 

                            <tr class="text-center">
                                <td>eventype : </td>

                                <td>{{ $row->funrun }} 
                                    <p> {{ $row->mini }} 
                                    <p>{{ $row->vip }}
                                    <p> {{ $row->half }}
                                    <p> {{ $row->vip }}
                                    <p> {{ $row->other1 }} 
                                    <p>{{ $row->other2 }}
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>gender : </td>
                                <td>{{ $row->gender }} </td>
                            </tr> 

                            <tr class="text-center">
                                <td>age : </td>
                                <td>
                                    <p> {{ $row->age1 }} 
                                    <p>{{ $row->age2 }}
                                    <p> {{ $row->age3 }}
                                    <p> {{ $row->age4 }}
                                    <p> {{ $row->age5 }} 
                                    <p>{{ $row->age6 }}
                                    <p> {{ $row->age7 }} 
                                    <p>{{ $row->age8 }}
                                    <p> {{ $row->age9 }}
                                    <p> {{ $row->age10 }}
                                    <p> {{ $row->age11 }} 
                                    <p>{{ $row->age12 }}
                                    <p> {{ $row->age13 }}
                                    <p> {{ $row->age14 }} 
                                    <p>{{ $row->age15 }}
                                    <p>{{ $row->age16 }}
                                </td>
                            </tr> 
                          
                            <tr class="text-center">
                                <td>size_shirts : </td>
                                <td>
                                    <p> {{ $row->size_shirts1 }} 
                                    <p> {{ $row->size_shirts2 }}
                                    <p> {{ $row->size_shirts3 }}
                                    <p> {{ $row->size_shirts4 }}
                                    <p> {{ $row->size_shirts5 }} 
                                    <p> {{ $row->size_shirts6 }}
                                    <p> {{ $row->size_shirts7 }} 
                                    
                                </td>
                            </tr> 

                            @php
                            $i++;
                            @endphp

                            @endforeach
                            @endif 
                            </tbody>
                          </table>
                        
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


 

    <script src="{{ asset('assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js')}}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>


    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/pdfmake.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/datatables-init.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable({
            "scrollX":'1280px',

            });
        });

        $('button[name="btnViewInfo"]').click(function(){
            location.href = ( '{{ url("Admin/view_admin") }}/'+$(this).data('id') );
        });

    </script>




     
        

</body>
</html>
