
@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

 <body class="wrapper"  style="background:#0c1567">

<!-- <div class="wrapper row2" id="event" style="background:#0c1567 ">
  <section class="hoc container clear" style="background:#0c1567 ">  -->
    <!-- ################################################################################################ -->
    <div class="sectiontitle" style="margin-top: 30px;">
      <h1 class="heading" style="color:#ffffff;">EventList 2018</h1>
      <h6 class="heading" style="color:#ffffff;"> กำหนดการ เปิดรับสมัคร ประจำปี 2018</h6>
    </div>

    <div class="container">
        <div class="row">
          @if (isset($event))
            @php
            $i=1;
            @endphp
            @foreach ($event as $key => $row) 

                <div class="col-md-4">
                    <div class="form-group" style="background: #ffffff;">
                    <div class="w3-container text-center" >
                        <h6 style="margin-top: 30px;"><b> {{ $row->name_evt }} </b></h6>
                    </div>    
                        <div class="w3-container">
                            <img src="{{URL::asset('/images/event/event2.png')}}" alt="profile Pic" height="200" width="200">
                        </div>
                    <hr>
                    <div class="w3-container text-center">
                        <h6>กำหนดการวิ่ง : {{ $row->opentdate }}</h6>
                        <p><a href="{{url('Event/detail_reg',$row->id)}}" class="btn btn-info"><i class="fa fa-eye"></i> ดูรายละเอียดการสมัคร </a></p>
                    </div>

                    <hr> 
                    </div> 
                </div>
            @php
            $i++;
            @endphp
            @endforeach
            @endif

          
        </div>
    </div>    
</body>      



<a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="{{asset('layout/scripts/jquery.min.js')}}"></script>
<script src="{{asset('layout/scripts/jquery.backtotop.js')}}"></script>
<!-- <script src="{{asset('layout/scripts/jquery.mobilemenu.js')}}"></script> -->
</body>
</html>


<link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
@endsection