@extends('layouts.app')

@section('content')



<link href="{{asset('layout/styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>


                <div class="ibox-content" style="display: block;">
                   
                          
                    <form method="POST" action="{{ route('accept') }}" aria-label="" id="" class="info"><!-- {{ route('accept') }} -->
                        @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                <p> ข้าพเจ้ารับรองว่า 
                                <p> ข้อมูลที่สมัครร่วมกิจกรรมในครั้งนี้เป็นความจริงทุกประการ ซึ่งข้าพเจ้ามีร่างกายสมบูรณ์พร้อมที่จะเข้าร่วมกิจกรรมอย่างเต็มใจ และข้าพเจ้าได้ศึกษาและยินยอมปฏิบัติตามกฎกติกาต่างๆ ทุกประการ โดยไม่เรียกร้องค่าเสียหายใด ๆ หากเกิดอันตรายหรือบาดเจ็บ ทั้งก่อน ระหว่าง และหลังการแข่งขัน นอกจากนี้ ข้าพเจ้ายินยอมให้คณะผู้จัดงานถ่ายภาพหรือวิดีโอ เพื่อบันทึกการจัดกิจกรรม และถือเป็นลิขสิทธิ์ของคณะผู้จัดงานในครั้งนี้ </p>

                                <p>(Wolver release and statement I certify that I am medically fit to complete and fully understand that I enter at my own risk and organizers will be no way be held responsibility for any injuries, illness or loss or as a result of the event.)</p>
                                </div>


                                <div class="col-lg-1 text-center">
                                   
                                </div>
                                 <div class="col-lg-11">
                                    <label>
                                        <input type="checkbox" id="accept" name="accept" value="accept" class="form-check-input" value="ยอมรับ"> ยอมรับ 
                                        
                                    </label>
                                </div>
                                 
                            </div>
                            <div class="col-lg-12 text-center">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" id="accept"> 
                                <i class="fa fa-floppy-o"></i> ตกลง
                                </button> 
                            </div>       
                    </form>
                </div>
        </div>
    </div>   
  </div>          


@endsection

