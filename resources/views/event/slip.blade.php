@extends('layouts.app')

@section('content')


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">
</head>

<body class="wrapper" id="page-top" style="background:#0c1567">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>


                <div class="ibox-content" style="display: block;">
                   
                          
                    <form method="POST" action="{{ route('accept') }}" aria-label="" id="" class="info"><!-- {{ route('accept') }} -->
                        @csrf
                <div class="row">
                  <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">99STEPRUN</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  <div class="card-title">
                                      <h3 class="text-center"> เลขที่ใบสมัคร</h3>
                                  </div>
                                  <hr>
                                  <form action="" method="post" novalidate="novalidate">
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                              <li class="list-inline-item">
                                              
                                          </ul>
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">ชื่ออีเวนท์ :</label>     
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">กำหนดการวิ่ง :</label>     
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">ชื่อ-สกุล :</label>
                                          
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">หมายเลขบัตรประชาชน /พาสปอร์ต :</label>
                                          
                                      </div>

                                      <div class="form-group has-success">
                                          <label for="cc-name" class="control-label mb-1">เบอร์โทรศัพท์ :</label>
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">เพศ : </label>     
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">ประเภทการสมัคร :</label>     
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">รุ่นอายุ :  ปี</label>     
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">ขนาดเสื้อ : </label>     
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">หมายเลขบิบ / BIB : </label>     
                                      </div>

                                      <div class="form-group">
                                          <label for="cc-number" class="control-label mb-1">ราคา : บาท</label>     
                                      </div>
                                     
                                      <div>
                                          <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                              <i class="fa fa-lock fa-lg"></i>&nbsp;
                                              <span id="payment-button-amount">Download</span>
                                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                                          </button>
                                      </div>
                                  </form>
                              </div>
                          </div>
                        </div>
                    </div> <!-- .card -->
                  </div>
                    <div class="col-lg-6">
                      <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Tranfer Money โอนเงินผ่านธนาคาร </strong>
                        </div>
                        <div class="card-body">
                          <div id="pay-invoice">
                              <div class="card-body">
                                  <div class="card-title">
                                      <h3 class="text-center"> Pay Invoice </h3>
                                  </div>
                                  <hr>
                              </div>
                         </div>

                              <form action="" method="post" novalidate="novalidate">
                                  <div class="form-group text-center">
                                    <ul class="list-inline">
                                      <li class="list-inline-item">
                                        <p> ธนาคาร​กสิกรไทย สาขา​ถนน​สุเทพ​  เชียงใหม่
                                        <p> ชื่อ​   นาย​ ศิระภัทร  สุริยะศักดิ์
                                        <p>เลขที่ บัญชี  <h6><span style="font-family: serif; color: #205698;" >471 2 312 772</h6>
                                      </li>
                                               
                                    </ul>
                                  </div>

                                  <div class="form-group">
                                      <label for="cc-payment" class="control-label mb-1">
                                      เลขที่ใบสมัคร</label>
                                      <input id="cc-pament" name="cc-payment" type="text" class="form-control" aria-required="true" aria-invalid="false" value="" required="">
                                  </div>

                                  <div class="form-group">
                                      <label for="cc-payment" class="control-label mb-1">
                                      เลขที่บัตรประชาชน/ID Card or Passport Number</label>
                                      <input id="cc-pament" name="cc-payment" type="text" class="form-control" aria-required="true" aria-invalid="false" value="" required="">
                                  </div>

                                  <div class="form-group">
                                      <label for="cc-payment" class="control-label mb-1">
                                      เวลาโอนเงิน ตัวอย่าง 12:15</label>
                                      <input id="cc-pament" name="cc-payment" type="text" class="form-control" aria-required="true" aria-invalid="false" value="" required="">
                                  </div>
                                  <div class="form-group">
                                      <label for="cc-payment" class="control-label mb-1">
                                      อัพโหลดสลิปเพื่อยืนยันการชำระเงิน / Upload Slip Payment</label>
                                      
                                      <input type="file" id="file-input" name="file-input" class="form-control-file" required="">
                                  </div>
                                  <div>
                                          <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                              <i class="fa fa-lock fa-lg"></i>&nbsp;
                                              <span id="payment-button-amount">Pay</span>
                                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                                          </button>
                                      </div>
                              </form>
                      </div>
                    </div>
                </div>
                                
                    </form>
                </div>
        </div>
    </div>   
  </div>          

</body>
</html>
@endsection

