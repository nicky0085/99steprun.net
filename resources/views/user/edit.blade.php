@extends('/layouts.app')

@section('content')

<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                            <div class="col-md-6" >
                                 <span><i class="fas fa-address-book"></i> 
                                    เพิ่มข้อมูลส่วนตัว
                                </span> 
                            </div>
                             <div class="col-md-6 text-right" >
                                <a class="btn btn-link" href="{{ route('view') }}">
                                    {{ __('กลับสู่หน้าหลัก') }}
                                </a>  
                            </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                 <form method="POST" action="{{ route('user') }}" aria-label="{{ __('user') }}" id="{{ Auth::user()->id }}">
                        @csrf

                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-10">
                            <label for="formGroupExampleInput2" class="alert-success">  
                                <h6 class="header"> กรุณากรอกข้อมูลส่วนตัวให้ครบถ้วน * </h6>
                            </label>

                            <div class="form-group" hidden="true">
                                <label for="formGroupExampleInput2"> ชื่อ :</label>
                                <input type="text" class="form-control" id="name_th" placeholder="Name" value="{{ Auth::user()->id }}" name="id">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput2"> ชื่อ :</label>
                                <input type="text" class="form-control" id="name_th" placeholder="Name" value="" name="name_th">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2"> สกุล :</label>
                                <input type="text" class="form-control" id="lastname" placeholder="Lastname" value="" name="lastname">
                            </div>
                           
                            <div class="form-group">
                                <p></p>
                                <label for="formGroupExampleInput2"> วัน เดือน ปี ที่เกิด : </label>
                                <div class="form-inline">
                                    <label for="formGroupExampleInput2" class="col-md-1"> วัน: </label>
                                    <input type="text" class="form-control col-md-2" id="day" placeholder="Day" value="" name="day">
                                    
                                    

                                    <label for="formGroupExampleInput2" class="col-md-1"> เดือน: </label>

                                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0 col-md-4" id="inlineFormCustomSelect" placeholder="Month" name="month">
                                    <option selected>Choose Month...</option>
                                    <option value="มกราคม">มกราคม</option>
                                    <option value="2">กุมภาพันธ์</option>
                                    <option value="กุมภาพันธ์">มีนาคม</option>
                                    <option value="เมษายน">เมษายน</option>
                                    <option value="พฤษภาคม">พฤษภาคม</option>
                                    <option value="มิถุนายน">มิถุนายน</option>
                                    <option value="กรกฏาคม">กรกฏาคม</option>
                                    <option value="สิงหาคม">สิงหาคม</option>
                                    <option value="กันยายน">กันยายน</option>
                                    <option value="ตุลาคม">ตุลาคม</option>
                                    <option value="พฤศจิกายน">พฤศจิกายน</option>
                                    <option value="ธันวาคม">ธันวาคม</option>
                                  </select>

                                    <label for="formGroupExampleInput2" class="col-md-1"> พ.ศ. </label>
                                    <input type="text" class="form-control col-md-2" id="Year" placeholder="Year" value="" name="year">
                                </div>
                            </div>
                            <div class="form-inline">
                                  <label class="mr-sm-2" for="inlineFormCustomSelect">กรุ๊ปเลือด :</label>
                                  <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect" placeholder="blood" name="bloodtype">
                                    <option selected>Choose Blood type...</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">O</option>
                                    <option value="D">AB</option>
                                  </select>
                            </div>

                            <p>
                             <div class="form-group" >
                                <label for="formGroupExampleInput2">ที่อยู่ :</label>
                                <input type="text" class="form-control" id="Address" placeholder="Address" value="" name="address">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput2"> เบอร์โทรศัพท์ :</label>
                                <input type="text" class="form-control" id="" placeholder="Telephone" value="" name="telephone">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2"> หมายเลขบัตรประชาชน :</label>
                                <input type="text" class="form-control" id="" placeholder="Idcard number" value="" name="Idcard">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2"> พาสปอร์ต (กรณีชาวต่างชาติ):</label>
                                <input type="text" class="form-control" id="" placeholder="Passport number" value="" name="passport">
                            </div>

                            <div class="form-inline">
                                <label class="mr-sm-2" for="inlineFormCustomSelect">ประเทศ :</label>
                                <input type="text" class="form-control" id="" placeholder="country" value="" name="country">
                            </div>

                            <p>
                             <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-info col-md-3" value="Save">
                                    Save 
                                 </button>


                                <button type="button" class="btn btn-danger col-md-3" value="reset" onclick="location.reload();"> Cancle </button>
                            </div>
                            <p>

                        </div>


                         <!-- <div class="col-md-6">
                            <label for="formGroupExampleInput2" class="alert-success">  
                                <h6 class="header"> ข้อมูลผู้ใช้งานระบบ * </h6>
                            </label>
                             <div class="form-group">
                                <label for="formGroupExampleInput">Email :</label>
                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input" value=" {{ Auth::user()->email }}" readonly name="email">
                              </div>

                              <div class="form-group">
                                <label for="formGroupExampleInput2"> Username :</label>
                                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input" value="{{ Auth::user()->name }}" readonly name="username">
                              </div>
                        </div> -->

                    </div>
                    <!-- end class row -->
                </from>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{asset('core/assets/js/form-elements.js', env('REDIRECT_HTTPS'))}}"></script>
<script src="{{asset('core/assets/js/form-validation.js', env('REDIRECT_HTTPS'))}}"></script>
<script src="{{asset('core/vendor/bootstrap-fileinput/jasny-bootstrap.js', env('REDIRECT_HTTPS'))}}"></script>
<script src="{{asset('core/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js', env('REDIRECT_HTTPS'))}}"></script>