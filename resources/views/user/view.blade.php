@extends('layouts.app')

@section('content')

<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        
                            <div class="col-md-6" >
                                 <span><i class="fas fa-address-book"></i>  ยินดีต้อนรับคุณ : {{ Auth::user()->name }} 
                                 | email: {{ Auth::user()->email }}
                                </span> 
                            </div>
                             <div class="col-md-6 text-right" >
                                <a class="btn btn-link" href="{{ route('edit') }}">
                                    {{ __('เพิ่มข้อมูลส่วนตัว') }}
                                </a>  
                            </div>
                       
                    </div>
                </div>
            </div>
            <p>
                <div class="scrollable">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="datatable">
                        <thead>

                                <tr class="info">
                                    <td><i class="fa-th-list fa-fw fas"></i> 
                                        <span> ข้อมูลส่วนตัว</span>
                                        <p><p>
                                        <p> ชื่อ - นามสกุล : </p>
                                        <p> วัน เดือน ปี เกิด : </p>
                                        <p> กลุ๊บเลือด :</p>
                                        <p> ที่อยู่ :</p>
                                        <p> เบอร์โทรศัพท์ :</p>
                                        <p> หมายเลขบัตรประชาชน :</p>
                                        <p> พาสปอร์ต :</p>
                                        <p> พาสปอร์ต :</p>
                                        <p> สัญชาติ :</p>
                                    </td>
                                </tr>
                        </thead>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>





@endsection
