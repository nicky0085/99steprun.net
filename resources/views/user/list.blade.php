@extends('layouts.app')

@section('content')

<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12 text-left">
                             <span><h6>ยินดีต้อนรับคุณ : {{ Auth::user()->name }}</h6> </span> 
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                <div class="scrollable">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="datatable">
                       
                            <thead>

                                @if (isset($event))
                                @php
                                  $i=1;
                                @endphp
                                @foreach ($event as $key => $row)
                                <tr class="info">
                                    <td>{{$i}}</td>
                                    <td><i class="fa-th-list fa-fw fas"></i> 
                                        <span>{{ $row->name_evt }}</span>
                                    </td>
                                    <td>กำหนดการวิ่ง {{ $row->opentdate }}</td>
                                    <td>รับสมัครจำนวน  <span style="color:green">{{ $row->limit }}</span> คน</td>
                                    <td>สถานะ : <span style="color:Green">เปิด</span>

                                    <td>
                                       

                                      <button class="btn btn-info btn-sm" type="button" name="btnViewInfo" data-id="{{ $row->id }}" class="btn btn-info"><i class="fa fa-eye"></i> รายละเอียด</button>
                                        </td>

                                  @php
                                      $i++;
                                    @endphp
                                </tr>
                                @endforeach
                              @endif
                            </thead>
                            
                    </table>

                </div>
            </div>
            </div>
        </div>
    </div>
</div>


@endsection


<script>
    jQuery(document).ready(function() {
     
   

    });

    

</script>