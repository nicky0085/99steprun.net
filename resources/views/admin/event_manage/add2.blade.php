
@extends('layouts.template')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<div class="container">
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <div class="col-md-6 ">
            <i class="fas fa-table"></i><b>สร้างอีเวนท์ </b> 
                 
          </div>
          <div class="col-md-6 text-right">
              <a href="{{url('Event_admin/view_event')}}" class="btn btn-danger">
                <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าอีเวนท์
              </a>
          </div>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
      
        <div class="card-header"> 
          <label for="exampleFormControlTextarea1">กรุณากรอกข้อมูลให้ครบถ้วน</label>
        </div> 
        <div class="col-md-12">
          <div class="row ">
          <div class="col-md-12"><p>

            <div class="form-control">
             <form method="POST" action="{{ route('save') }}" aria-label="{{ __('save') }}">
                @csrf  
                  <div class="row">

                    <div class="col" id="" style="margin-top:30px;">
                      <div class="form-control">
                        <div class="form-group ">
                          <input type="text" class="form-control" name="group_id1" id="1" value="1" required=""  placeholder="" hidden=""> 

                          <label for="exampleFormControlTextarea1"><b>ประเภทการแข่งขัน 1</b></label>
                          <input type="text" class="form-control" name="event_type1" id="event_type" value=""   placeholder="ประเภทการแข่งขัน">    
                        </div>

                        <div class="form-group ">
                          <label>ราคา : </label>
                          <input type="text" class="form-control" name="price1" id="price1" value=""  placeholder="ราคา ">     
                        </div>

                        <div class="form-inline" style="margin-top: 30px;" >

                        <div class="form-check" style="margin-left: 10px;">
                          <input type="radio" class="form-check-input" id="gender1" name="gender1" value="แบ่งชายหญิง">
                          <label class="form-check-label" for="materialUnchecked"> แบ่งชายหญิง </label>
                        </div>
                        <div class="form-check" style="margin-left: 10px;">
                          <input type="radio" class="form-check-input" id="gender1" name="gender1" checked value="ไม่แบ่งชายหญิง">
                          <label class="form-check-label" for="materialChecked"> ไม่แบ่งชายหญิง </label>
                        </div>
                        </div>


                        <div class="form-inline" style="margin-top: 30px;" >
                        <div class="form-check" style="margin-left: 10px;">
                          <input type="radio" class="form-check-input" id="shirts1" name="shirts1" value="Y">
                          <label class="form-check-label" for="materialUnchecked"> มีเสื้อ </label>
                        </div>
                        <div class="form-check" style="margin-left: 10px;">
                          <input type="radio" class="form-check-input" id="shirts1" name="shirts1" checked value="N">
                          <label class="form-check-label" for="materialChecked"> ไม่มีเสื้อ </label>
                        </div>
                        </div>


                      <div class="row" style="margin-top: 30px;">

                            <div class="col-md-3">
                              <p>
                              <input type="text" class="form-control" name="age1" id="age" value=""   placeholder="รุ่นอายุ1"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age2" id="age" value=""   placeholder="รุ่นอายุ2"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age3" id="age" value=""   placeholder="รุ่นอายุ3"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age4" id="age" value=""   placeholder="รุ่นอายุ4"> 
                            </div>

                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age5" id="age" value=""   placeholder="รุ่นอายุ5"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age6" id="age" value=""   placeholder="รุ่นอายุ6"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age7" id="age" value=""   placeholder="รุ่นอายุ7"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age8" id="age" value=""   placeholder="รุ่นอายุ8"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age9" id="age" value=""   placeholder="รุ่นอายุ9"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age10" id="age" value=""   placeholder="รุ่นอายุ10"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age11" id="age" value=""   placeholder="รุ่นอายุ11"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age12" id="age" value=""   placeholder="รุ่นอายุ12"> 
                            </div><div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age13" id="age" value=""   placeholder="รุ่นอายุ13"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age14" id="age" value=""   placeholder="รุ่นอายุ14"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age15" id="age" value=""   placeholder="รุ่นอายุ15"> 
                            </div>
                            <div class="col-md-3"><p>
                              <input type="text" class="form-control" name="age16" id="age" value=""   placeholder="รุ่นอายุ16"> 
                            </div>


                      </div>

                      </div>

                   

                   <div class="text-center" style="margin-top: 100px;">
                    
                      <button type="submit" name="submit" value="submit" class="btn btn-primary" id="save">save</button>
                      <button class="btn btn-danger" onclick="">cancle</button>
                      <p>
                    
                    </div>
                   </div>
                 </div>
               
                 
      
              </form>

              </div>
              
          </div>
        </div>

      </div>

    </div> 
  </div>
</div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center" >
         <!--  <small style="color:ffffff;" >www.99steprun.com</small> -->
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>



    
  </div>
</body>

<script type="text/javascript">
  $(document).ready(function() {
    var max_fields      = 15; //maximum input boxes allowed
    var wrapper         = $(".add_age"); //Fields wrapper
    var add_button      = $(".btnAdd"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-inline" style="margin-top: 30px;"><input type="text" class="form-control" name="age" placeholder="รุ่นอายุ" /><a href="#" class="btn btn-danger" id="remove_field" ><i class="fas fa-trash-alt"></i> </a> </div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click","#remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

</html>
@endsection
