
@extends('layouts.template')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<div class="container">
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <div class="col-md-6 ">
            <i class="fas fa-table"></i><b>รายละเอียดอีเว้นท์ </b> 
                 
          </div>
          <div class="col-md-6 text-right">
              <a href="{{url('Event_admin/view_event')}}" class="btn btn-danger">
                <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าอีเวนท์
              </a>
          </div>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
      
        <div class="card-header"> </div> 
        <div class="col-md-12">
          <div class="row ">
              <div class="col-md-12"><p>
                   @if (isset($event_c))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_c as $key => $row)

                        <div class="row">
                            <div class="col-lg-5">
                              <h6>{{$row->name_evt}}</h6>
                            </div>
                            <div class="col-lg-4">
                              <label>กำหนดการวิ่ง {{$row->opentdate}} </label>
                            </div>
                            <div class="col-lg-3">
                                <label>จำนวนรับสมัคร {{$row->limit}} คน </label>
                            </div>
                        </div>

                        <div class="row" style="background: #f4f4f4;"> 
                              <div class="col-lg-12">
                                <div class="form-control" style="margin-top: 20px;">
                                {{ $row->file}}
                                </div>
                            </div>
                        </div>

                        <div class="row" style="background: #f4f4f4;">
                              <div class="col-lg-12">
                                <div class="form-control" style="margin-top: 20px;">
                                {{ $row->description}}
                                </div>
                            </div>
                        </div>

                    @php
                    $i++;
                    @endphp

                    @endforeach
                    @endif

                    <div class="row" style="background: #f4f4f4;">
                      @if (isset($event_g1))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_g1 as $key => $g1)

                            <div class="col-lg-4">
                                <div class="form-control" style="margin-top: 20px;">
                                  <label><h6>ประเภทการแข่งขัน : {{ $g1->event_type}}</h6></label>
                                  <p><label>ราคา : {{ $g1->price}}</label><br>
                                  <label>เพศ : {{ $g1->gender}}</label>
                                  <p><label>รุ่นอายุ</p></p>
                                  <p>{{ $g1->age1}}</p>
                                  <p>{{ $g1->age2}}</p>
                                  <p>{{ $g1->age3}}</p>
                                  <p>{{ $g1->age4}}</p>
                                  <p>{{ $g1->age5}}</p>
                                  <p>{{ $g1->age6}}</p>
                                  <p>{{ $g1->age7}}</p>
                                  <p>{{ $g1->age8}}</p>
                                  <p>{{ $g1->age9}}</p>
                                  <p>{{ $g1->age10}}</p>
                                  <p>{{ $g1->age11}}</p>
                                  <p>{{ $g1->age12}}</p>
                                  <p>{{ $g1->age13}}</p>
                                  <p>{{ $g1->age14}}</p>
                                  <p>{{ $g1->age15}}</p>
                                  <p>{{ $g1->age16}}</p>
                                </div>
                            </div>

                        @php
                        $i++;
                        @endphp

                        @endforeach
                        @endif

                       @if (isset($event_g2))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_g2 as $key => $g2)

                            <div class="col-lg-4">
                                <div class="form-control" style="margin-top: 20px;">
                                  <label><h6>ประเภทการแข่งขัน : {{ $g2->event_type}}</h6></label>
                                  <p><label>ราคา : {{ $g2->price}}</label><br>
                                  <label>เพศ : {{ $g2->gender}}</label>
                                  <p><label>รุ่นอายุ</p></p>
                                  <p>{{ $g2->age1}}</p>
                                  <p>{{ $g2->age2}}</p>
                                  <p>{{ $g2->age3}}</p>
                                  <p>{{ $g2->age4}}</p>
                                  <p>{{ $g2->age5}}</p>
                                  <p>{{ $g2->age6}}</p>
                                  <p>{{ $g2->age7}}</p>
                                  <p>{{ $g2->age8}}</p>
                                  <p>{{ $g2->age9}}</p>
                                  <p>{{ $g2->age10}}</p>
                                  <p>{{ $g2->age11}}</p>
                                  <p>{{ $g2->age12}}</p>
                                  <p>{{ $g2->age13}}</p>
                                  <p>{{ $g2->age14}}</p>
                                  <p>{{ $g2->age15}}</p>
                                  <p>{{ $g2->age16}}</p>
                                </div>
                            </div>

                        @php
                        $i++;
                        @endphp

                        @endforeach
                        @endif


                      @if (isset($event_g3))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_g3 as $key => $g3)

                            <div class="col-lg-4">
                                <div class="form-control" style="margin-top: 20px;">
                                  <label><h6>ประเภทการแข่งขัน : {{ $g3->event_type}}</h6></label>
                                  <p><label>ราคา : {{ $g3->price}}</label><br>
                                  <label>เพศ : {{ $g3->gender}}</label>
                                  <p><label>รุ่นอายุ</p></p>
                                  <p>{{ $g3->age1}}</p>
                                  <p>{{ $g3->age2}}</p>
                                  <p>{{ $g3->age3}}</p>
                                  <p>{{ $g3->age4}}</p>
                                  <p>{{ $g3->age5}}</p>
                                  <p>{{ $g3->age6}}</p>
                                  <p>{{ $g3->age7}}</p>
                                  <p>{{ $g3->age8}}</p>
                                  <p>{{ $g3->age9}}</p>
                                  <p>{{ $g3->age10}}</p>
                                  <p>{{ $g3->age11}}</p>
                                  <p>{{ $g3->age12}}</p>
                                  <p>{{ $g3->age13}}</p>
                                  <p>{{ $g3->age14}}</p>
                                  <p>{{ $g3->age15}}</p>
                                  <p>{{ $g3->age16}}</p>
                                </div>
                            </div>

                        @php
                        $i++;
                        @endphp

                        @endforeach
                        @endif
                        </div>


                        <div class="row">
                          <hr>
                        </div>
                  </div>
                  

              </div>
              
          </div>
        </div>

      </div>

    </div> 
  </div>
</div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center" >
         <!--  <small style="color:ffffff;" >www.99steprun.com</small> -->
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>



    
  </div>
</body>

<script type="text/javascript">
  $(document).ready(function() {
    var max_fields      = 15; //maximum input boxes allowed
    var wrapper         = $(".add_age"); //Fields wrapper
    var add_button      = $(".btnAdd"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-inline" style="margin-top: 30px;"><input type="text" class="form-control" name="age" placeholder="รุ่นอายุ" /><a href="#" class="btn btn-danger" id="remove_field" ><i class="fas fa-trash-alt"></i> </a> </div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click","#remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

</html>
@endsection
