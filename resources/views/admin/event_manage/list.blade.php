
@extends('layouts.template')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">

      
  
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
 
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <div class="col-md-6 ">
            <i class="fas fa-table"></i><b> รายการอีเวนท์ </b> 
                
          </div>
          <div class="col-md-6 text-right">
             
          </div>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">  
            <form method="POST" action="{{ route('generate_code') }}" aria-label="{{ __('generate_code') }}">
                        @csrf                    
                        <div class="row"> 
                            <div class="col-lg-12 text-right">
                                <a href="{{ url('Event_admin/add_event') }}" class="btn btn-primary">
                                <i class="fas fa-plus"></i> สร้างอีเวนท์ </a>
                            </div>
                        </div>      
            </form>           
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead class="text-center">
                <tr >
                  <th style="background-color:#FFFFFF; color: black;">ที่</th>
                  <th style="background-color:#FFFFFF; color: black;">ชื่ออีเวนท์</th>
                  <th style="background-color:#FFFFFF; color: black;">กำหนดการวิ่ง</th>
                  <th style="background-color:#FFFFFF; color: black;">จำนวนรับสมัคร / คน</th>
                  <th style="background-color:#FFFFFF; color: black;">เปิด / ปิด</th>
                  <th style="background-color:#FFFFFF; color: black;">สร้าง VIP โค้ด</th>
                  <th style="background-color:#FFFFFF; color: black;">รายละเอียด</th>
                  <th style="background-color:#FFFFFF; color: black;">แก้ไข</th>
                  <th style="background-color:#FFFFFF; color: black;">ลบ</th>
                </tr>
              </thead>
             
              <tbody class="text-left">
                
               @if (isset($event_m))
                                @php
                                  $i=1;
                                @endphp
                                @foreach ($event_m as $key => $row)
                                <tr class="info">
                                    <td>{{$i}}</td>
                                    <td><i class="fa-th-list fa-fw fas"></i> 
                                        <span>{{ $row->name_evt }}</span>
                                    </td>
                                    <td> {{ $row->opentdate }}</td>
                                    <td>  <span style="color:green">{{ $row->limit }}</span> </td>
                                    <td class="text-center">
                                    <label>เปิด</label>
                                    </td>
                                    <td class="text-center"> 
                                        <a href="{{ url('Event_admin/vip_event', $row->id) }}" class="btn btn-primary"><i class="fas fa-barcode"></i> VIP </a>
                                   </td>
                                   <td class="text-center">
                                    <a href="{{ url('Event_admin/detail_event', $row->id) }}" class="btn btn-info"><i class="fa fa-eye"></i> ดูรายละเอียด </a>
                                    </td>
                                    <td class="text-center">
                                    <a href="" class="btn btn-warning"><i class="far fa-sun"></i> แก้ไข </a>
                                    </td>
                                    <td class="text-center">
                                    <a href="" class="btn btn-danger"><i class="far fa-trash-alt"></i> ลบ </a>
                                   </td> 
                                   

                                  @php
                                      $i++;
                                    @endphp
                                </tr>
                                @endforeach
                              @endif
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center" >
         <!--  <small style="color:ffffff;" >www.99steprun.com</small> -->
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    


    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>



  </div>
</body>

</html>


@endsection
