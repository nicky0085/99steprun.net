
@extends('layouts.template')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
 
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <div class="col-md-6 ">
            <i class="fas fa-table"></i><b> รายงานการสมัคร อีเวนท์ </b>                   
          </div>
          <div class="col-md-6 text-right">
              
          </div>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">  
            
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr >
                          <th style="background-color:#FFFFFF; color: black;">ที่</th>
                          <th style="background-color:#FFFFFF; color: black;">ชื่ออีเวนท์</th>
                          <th style="background-color:#FFFFFF; color: black;">กำหนดการวิ่ง</th>
                          <th style="background-color:#FFFFFF; color: black;">จำนวน</th>
                          <th style="background-color:#FFFFFF; color: black;">เปิด / ปิด</th>
                          <th style="background-color:#FFFFFF; color: black;">รายชื่อผู้สมัคร VIP</th>
                          <th style="background-color:#FFFFFF; color: black;">รายชื่อผู้สมัคร</th>
                          
                        </tr>
                      </thead>
                        <tbody class="text-left">
                
                            @if (isset($event))
                                @php
                                  $i=1;
                                @endphp
                                @foreach ($event as $key => $row)
                                <tr class="info">
                                    <td>{{$i}}</td>
                                    <td><i class="fa-th-list fa-fw fas"></i> 
                                        <span>{{ $row->name_evt }}</span>
                                    </td>
                                    <td>กำหนดการวิ่ง {{ $row->opentdate }}</td>
                                    <td>  <span style="color:green">{{ $row->limit }}</span> คน</td>
                                    <td class="text-center">
                                    <label>เปิด</label>
                                    </td>
                                    <td class="text-center"> 
                                        <a href="" class="btn btn-primary"><i class="fas fa-barcode"></i> รายชื่อผู้สมัคร vip</a>
                                   </td>
                                    <td><a href="{{ url('Event_admin/report_register', $row->id) }}" class="btn btn-info"><i class="fa fa-eye"></i> รายชื่อผู้สมัคร </a>
                                    </td> 
                                    
                                   

                                  @php
                                      $i++;
                                    @endphp
                                </tr>
                                @endforeach
                              @endif
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center" >
         <!--  <small style="color:ffffff;" >www.99steprun.com</small> -->
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>

    
  </div>
</body>

</html>
@endsection
