
@extends('layouts.template')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<div class="container">
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <div class="col-md-6 ">
            <i class="fas fa-table"></i><b>รายละเอียดอีเว้นท์ </b> 
  @if (isset($event_c))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_c as $key => $row)               
          </div>
          <div class="col-md-6 text-right">
              <a href="{{url('Event_admin/report_register',$row->id )}}" class="btn btn-danger">
                <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าอีเวนท์
              </a>
          </div>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
      
        <div class="card-header"> </div> 
        <div class="col-md-12">
          <div class="row ">

              <div class="col-md-12"><p>
                  

                        <div class="row">
                            <div class="col-lg-5">
                              <h6>{{$row->name_evt}}</h6>
                            </div>
                            <div class="col-lg-4">
                              <label>กำหนดการวิ่ง {{$row->opentdate}} </label>
                            </div>
                            <div class="col-lg-3">
                                <label>จำนวนรับสมัคร {{$row->limit}} คน </label>
                            </div>
                        </div>

                        <div class="row" style="background: #f4f4f4;"> 
                              <div class="col-lg-12">
                                <div class="form-control" style="margin-top: 20px;">
                                {{ $row->file}}
                                </div>
                            </div>
                        </div>

                        <div class="row" style="background: #f4f4f4;">
                              <div class="col-lg-12">
                                <div class="form-control" style="margin-top: 20px;">
                                {{ $row->description}}
                                </div>
                            </div>
                        </div>

                    @php
                    $i++;
                    @endphp

                    @endforeach
                    @endif

                    <div class="row" style="background: #f4f4f4;">
                      @if (isset($event_g1))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_g1 as $key => $g1)

                            <div class="col-lg-4">
                                <div class="form-control" style="margin-top: 20px;">
                                  <label><h6><b>ประเภทการแข่งขัน :</b> {{ $g1->event_type}}</h6></label>
                                  <p><label><b>ราคา : </b> {{ $g1->price}}</label><br>
                                  <label><b>เพศ : </b> {{ $g1->gender}}</label>
                                  <p><label><b>รุ่นอายุ</b></p></p>
                                   @php
                                  if($g1->age1 == null){
                                      echo "";
                                  }else{
                                      echo '<div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g1->age1.'</label></div>';
                                  }
                                  if($g1->age2 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g1->age2.'</label></div>';
                                  }

                                  if($g1->age3 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g1->age3.'</label></div>';
                                  }

                                  if($g1->age4 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age4">
                                        <label class="form-check-label">'.$g1->age4.'</label></div>';
                                  }
                                  if($g1->age5 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age5.'</label></div>';
                                  }

                                  if($g1->age6 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age6.'</label></div>';
                                  }

                                  if($g1->age7 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age7.'</label></div>';
                                  }

                                  if($g1->age8 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age8.'</label></div>';
                                  }

                                  if($g1->age9 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age9.'</label></div>';
                                  }

                                  if($g1->age10 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age10.'</label></div>';
                                  }

                                  if($g1->age11 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age11.'</label></div>';
                                  }

                                  if($g1->age12 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age12.'</label></div>';
                                  }

                                  if($g1->age13 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age13.'</label></div>';
                                  }

                                  if($g1->age14 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age14.'</label></div>';
                                  }

                                  if($g1->age15 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age15.'</label></div>';
                                  }

                                  if($g1->age16 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g1->age16.'</label></div>';
                                  }


                                  if($g1->shirts == "Y"){
                                  echo ' <div class="col-lg-12"><div class="form-group"> <label for="team">ขนาด เสื้อ / size</label><select class="form-control" name="t_size_id" id="t_size_id" required="">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="XS 34">XS (รอบอก 34)</option>
                                        <option value="S 36">S (รอบอก 36)</option>
                                        <option value="M 38">M (รอบอก 38)</option>
                                        <option value="L 40">L (รอบอก 40)</option>
                                        <option value="XL 42">XL (รอบอก 42)</option>
                                        <option value="2XL 44">2XL (รอบอก 44)</option>
                                        <option value="3XL 46">3XL (รอบอก 46)</option>
                                        <option value="4XL 48">4XL (รอบอก 48)</option>
                                      </select></div></div>';
                                  }else{
                                    echo "";
                                  }

                                  @endphp
                                </div>
                            </div>

                        @php
                        $i++;
                        @endphp

                        @endforeach
                        @endif

                       @if (isset($event_g2))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_g2 as $key => $g2)

                            <div class="col-lg-4">
                                <div class="form-control" style="margin-top: 20px;">
                                  <label><h6><b>ประเภทการแข่งขัน : </b>{{ $g2->event_type}}</h6></label>
                                  <p><label><b>ราคา : </b>{{ $g2->price}}</label><br>
                                  <label><b>พศ : </b>{{ $g2->gender}}</label>
                                  <p><label><b>รุ่นอายุ</b></p>
                                  @php
                                  if($g2->age1 == null){
                                      echo "";
                                  }else{
                                      echo '<div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g2->age1.'</label></div>';
                                  }
                                  if($g2->age2 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g2->age2.'</label></div>';
                                  }

                                  if($g2->age3 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g2->age3.'</label></div>';
                                  }

                                  if($g2->age4 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age4">
                                        <label class="form-check-label">'.$g2->age4.'</label></div>';
                                  }
                                  if($g2->age5 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age5.'</label></div>';
                                  }

                                  if($g2->age6 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age6.'</label></div>';
                                  }

                                  if($g2->age7 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age7.'</label></div>';
                                  }

                                  if($g2->age8 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age8.'</label></div>';
                                  }

                                  if($g2->age9 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age9.'</label></div>';
                                  }

                                  if($g2->age10 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age10.'</label></div>';
                                  }

                                  if($g2->age11 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age11.'</label></div>';
                                  }

                                  if($g2->age12 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age12.'</label></div>';
                                  }

                                  if($g2->age13 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age13.'</label></div>';
                                  }

                                  if($g2->age14 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age14.'</label></div>';
                                  }

                                  if($g2->age15 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age15.'</label></div>';
                                  }

                                  if($g2->age16 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g2->age16.'</label></div>';
                                  }

                                  if($g2->shirts == "Y"){
                                  echo ' <div class="col-lg-12"><div class="form-group"> <label for="team">ขนาด เสื้อ / size</label><select class="form-control" name="t_size_id" id="t_size_id" required="">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="XS 34">XS (รอบอก 34)</option>
                                        <option value="S 36">S (รอบอก 36)</option>
                                        <option value="M 38">M (รอบอก 38)</option>
                                        <option value="L 40">L (รอบอก 40)</option>
                                        <option value="XL 42">XL (รอบอก 42)</option>
                                        <option value="2XL 44">2XL (รอบอก 44)</option>
                                        <option value="3XL 46">3XL (รอบอก 46)</option>
                                        <option value="4XL 48">4XL (รอบอก 48)</option>
                                      </select></div></div>';
                                  }else{
                                    echo "";
                                  }
                                 

                                  @endphp
                                </div>
                            </div>

                        @php
                        $i++;
                        @endphp

                        @endforeach
                        @endif


                      @if (isset($event_g3))
                       @php
                        $i=1;
                        @endphp
                        @foreach ($event_g3 as $key => $g3)

                            <div class="col-lg-4">
                                <div class="form-control" style="margin-top: 20px;">
                                  <label><h6><b>ประเภทการแข่งขัน : {{ $g3->event_type}}</b></h6></label>
                                  <p><label><b>ราคา : {{ $g3->price}}</b></label><br>
                                  <label><b>เพศ : {{ $g3->gender}}</b></label>
                                  <p><label><b>รุ่นอายุ</b></p></p>

                                  @php
                                  if($g3->age1 == null){
                                      echo "";
                                  }else{
                                      echo '<div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g3->age1.'</label></div>';
                                  }
                                  if($g3->age2 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g3->age2.'</label></div>';
                                  }

                                  if($g3->age3 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label">'.$g3->age3.'</label></div>';
                                  }

                                  if($g3->age4 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age4">
                                        <label class="form-check-label">'.$g3->age4.'</label></div>';
                                  }
                                  if($g3->age5 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age5.'</label></div>';
                                  }

                                  if($g3->age6 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age6.'</label></div>';
                                  }

                                  if($g3->age7 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age7.'</label></div>';
                                  }

                                  if($g3->age8 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age8.'</label></div>';
                                  }

                                  if($g3->age9 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age9.'</label></div>';
                                  }

                                  if($g3->age10 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age10.'</label></div>';
                                  }

                                  if($g3->age11 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age11.'</label></div>';
                                  }

                                  if($g3->age12 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age12.'</label></div>';
                                  }

                                  if($g3->age13 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age13.'</label></div>';
                                  }

                                  if($g3->age14 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age14.'</label></div>';
                                  }

                                  if($g3->age15 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age15.'</label></div>';
                                  }

                                  if($g3->age16 == null){
                                      echo "";
                                  }else{
                                      echo '<p><div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="$g3->age5">
                                        <label class="form-check-label">'.$g3->age16.'</label></div>';
                                  }

                                

                                  if($g3->shirts == "Y"){
                                  echo ' <div class="col-lg-12"><div class="form-group"> <label for="team">ขนาด เสื้อ / size</label><select class="form-control" name="t_size_id" id="t_size_id" required="">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="XS 34">XS (รอบอก 34)</option>
                                        <option value="S 36">S (รอบอก 36)</option>
                                        <option value="M 38">M (รอบอก 38)</option>
                                        <option value="L 40">L (รอบอก 40)</option>
                                        <option value="XL 42">XL (รอบอก 42)</option>
                                        <option value="2XL 44">2XL (รอบอก 44)</option>
                                        <option value="3XL 46">3XL (รอบอก 46)</option>
                                        <option value="4XL 48">4XL (รอบอก 48)</option>
                                      </select></div></div>';
                                  }else{
                                    echo "";
                                  }
                                  @endphp
                                 
                                  
                                </div>
                            </div>

                        @php
                        $i++;
                        @endphp

                        @endforeach
                        @endif


                        <div class="col-lg-12" style="margin-top: 30px;">
                          <label>กรุณากรอกข้อมูลการสมัครให้ครบถ้วน </label>
                        <div class="form-control">
                          
                        <form method="POST" action="http://run.payap.ac.th/Event/add_reg" aria-label="add_reg">
                        <input type="hidden" name="_token" value="">                    
                        <div class="row"> 
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="firstname">ชื่อ/First Name <font color="#FB0004"></font></label>
                                        <input type="text" class="form-control" name="firstname" id="firstname" value="" required="">     
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="lastname">นามสกุล/Last Name <font color="#FB0004"></font></label>
                                    
                                        <input type="text" class="form-control" name="lastname" id="lastname" value="" required="">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="idcard">เลขที่บัตรประชาชน/ID Card or Passport Number</label>
                                    
                                        <input type="text" class="form-control" name="idcard" id="idcard" value="" maxlength="13" required="">
                                </div> 
                            </div>


                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="birthdate">วัน/เดือน/ปี เกิด / Birth Date </label>
                                       <div class="input-group">
                                        <label class="input-group-addon" for="birthdate">
                                            
                                        </label><input type="text" id="" class="datepickerthai form-control" name="birthdate" value="" required="">
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="team">ทีม/Team </label>
                                        <input type="text" class="form-control" name="team" id="team" value="">
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="phone">เบอร์โทรศัพท์/Phone Number </label>
                                        <input type="text" class="form-control" name="phone" id="phone" value="" required="">
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="email">E-mail </label>
                                        <input type="email" class="form-control" name="email" id="email" value="" required="">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="address">ที่อยู่/Address </label>
                                        <textarea name="address" id="address" rows="3" class="form-control" required=""></textarea>
                                </div>
                            </div>

                            <div class="col-lg-12">
                              <div class="form-group form-check-inline">
                                  <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="">
                                   <label class="form-check-label">   หญิง  </label>
                                                                
                                  <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="">
                                   <label class="form-check-label">   ชาย  </label>
                              </div>
                            </div>

                            
                    <p></p></div> <!-- end class row -->
                    <div class="text-center">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary" id="accept"> 
                            <i class="fa fa-floppy-o"></i> บันทึก/Save
                        </button> 

                        <button type="button" name="back" value="back" class="btn btn-info" onclick="window.location='?'"><i class="fa fa-home"></i> ล้างค่า/Clear</button>
                    </div>
                  </form>

                   </div>
                  </div>
                </div>
                <hr>
                


                

              </div>
            </div>
              
          </div>
        </div>

      </div>

    </div> 
  </div>
</div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center" >
         <!--  <small style="color:ffffff;" >www.99steprun.com</small> -->
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>



    
  </div>
</body>

<script type="text/javascript">
  $(document).ready(function() {
    var max_fields      = 15; //maximum input boxes allowed
    var wrapper         = $(".add_age"); //Fields wrapper
    var add_button      = $(".btnAdd"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-inline" style="margin-top: 30px;"><input type="text" class="form-control" name="age" placeholder="รุ่นอายุ" /><a href="#" class="btn btn-danger" id="remove_field" ><i class="fas fa-trash-alt"></i> </a> </div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click","#remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

</html>
@endsection
