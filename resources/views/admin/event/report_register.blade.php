
@extends('layouts.template')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>99STEPRUN</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css" rel="stylesheet')}}">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
 
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <div class="col-md-6 ">
            <i class="fas fa-table"></i><b> รายงานการสมัคร อีเวนท์ </b> 
                            @if (isset($event))
                                @php
                                  $i=1;
                                @endphp
                                @foreach ($event as $key => $evt)
                                    
                                     <label>  {{ $evt->name_evt}} </label>
                                   
                                 @php
                                      $i++;
                                    @endphp
                                </tr>
                                @endforeach
                            @endif   
          </div>
          <div class="col-md-6 text-right">
              <a href="{{url('Event_admin/report_event')}}" class="btn btn-danger">
                <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าอีเวนท์
              </a>
          </div>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">  
          
          <div class="col-md-12 text-right">
            <a href="{{url('Event_admin/add_register',$evt->id)}}" class="btn btn-primary"><i class="fas fa-plus"></i> เพิ่มรายชื่อ</a>
            <a href="" class="btn btn-success"><i class="fas fa-table"></i> export xls. </a>
          </div>
              
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead class="text-center">
                                <tr class="text-center">
                                    <th  style="background-color:#FFFFFF; color: black;">ที่</th>
                                    <th  style="background-color:#FFFFFF; color: black;">ชื่อ</th>
                                    <th  style="background-color:#FFFFFF; color: black;">สกุล</th>
                                    <th  style="background-color:#FFFFFF; color: black;">เบอร์โทรศัพท์</th>
                                    <th  style="background-color:#FFFFFF; color: black;">วันเดือปีเกิด</th>
                                    <th  style="background-color:#FFFFFF; color: black;">ทีม</th>
                                    <th  style="background-color:#FFFFFF; color: black;"> เลขที่บัตรประชาชน / พาสปอร์ต </td>
                                    <th  style="background-color:#FFFFFF; color: black;">ที่อยู่</th>
                                    <th  style="background-color:#FFFFFF; color: black;">อีเมล</th>
                                    <th  style="background-color:#FFFFFF; color: black;">ประเภทการแข่งขัน</th>
                                    <th  style="background-color:#FFFFFF; color: black;">เพศ</th>
                                    <th  style="background-color:#FFFFFF; color: black;">รุ่นอายุ</th>
                                    <th  style="background-color:#FFFFFF; color: black;">ขนาดเสื้อ</th>
                                    <th  style="background-color:#FFFFFF; color: black;">บิบ</th>
                                    <th  style="background-color:#FFFFFF; color: black;">สถานะ</th>
                                    <th  style="background-color:#FFFFFF; color: black;">action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @if (isset($register))
                                @php
                                  $i=1;
                                @endphp
                                @foreach ($register as $key => $row)
                                <tr class="info">
                                    <td>{{$i}}</td>
                                    <td><span>{{ $row->name }}</span> </td>
                                    <td><span>{{ $row->lastname }}</span></td>
                                    <td> <span>{{ $row->telephone }}</span></td>
                                    <td><span>{{ $row->birthday }}</span></td>
                                    <td><span>{{ $row->team }}</span></td>
                                    <td><span>{{ $row->idcard_passport }}</span></td>
                                    <td><span>{{ $row->address }}</span></td>
                                    <td><span>{{ $row->email }}</span></td>
                                    <td><span>{{ $row->evt_type }}</span></td>
                                    <td><span>{{ $row->gender }}</span></td>
                                    <td><span>{{ $row->age }}</span></td>
                                    <td><span>{{ $row->size_shirts }}</span></td>
                                    <td>@php echo $new_index = str_pad($i, 4, "0", STR_PAD_LEFT);  @endphp </td>
                                    <td style="color:green;"></td>

                                    <td>
                                        
                                        <label><a href="" class="btn btn-warning"><i class="far fa-sun"></i> แก้ไข </a></label>
                                         <label><a href="" class="btn btn-danger"><i class="far fa-trash-alt"></i> ลบ </a></label>

                                    </td> 

                                  @php
                                      $i++;
                                    @endphp
                                </tr>
                                @endforeach
                              @endif
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center" >
         <!--  <small style="color:ffffff;" >www.99steprun.com</small> -->
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>

    
  </div>
</body>

</html>
@endsection
