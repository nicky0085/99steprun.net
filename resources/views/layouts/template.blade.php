<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
<title>99STEPRUN</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link href="{{asset('layout/styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">
 <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body id="top">

<div class="wrapper row0" style="background-color: #ffffff;text-align: center;">
  <header id="header" class="hoc clear" > 
    <!-- ################################################################################################ -->
    <div id="logo" class="one_quarter first">
      <h1><a href="#"><span style="font-family: serif; color: #205698;">99</span ><span style="color:#95103B;">STEP</span><span style="color: #205698;">RUN</a></h1>
    </div>
    <div class="three_quarter" ">
      <ul class="nospace clear">
        <li class="one_third first" >
          <div class="block clear"><a href="#"><i class="fas fa-phone"></i></a> <span><strong>LINE @ PRC Running Club</strong><a href="https://line.me/R/ti/p/%40ike2496p">https://line.me/R/ti/p/%40ike2496p</a></span></div>
        </li>
        <li class="one_third">
          <div class="block clear"><a href="#"><i class="fas fa-envelope"></i></a> <span><strong>Send us a mail:</strong>prc.runningclub@gmail.com</span></div>
        </li>
        <li class="one_third" >
          <div class="block clear"><img src="images/demo/backgrounds/logo01.png" alt=""></a></div>
        </li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
</div>

<div class="wrapper row1" style="background-color: #205698;">
  <section class="hoc clear"> 
    <nav id="mainav">
      <ul class="clear">
        @guest
            <li class="nav-item"><a href=""><i class="fas fa-home"></i> หน้าแรก</a></li>
            <li class="nav-item"><a href=""><i class="fas fa-list"></i> งานอีเวนท์</a></li>
            <li class="nav-item"><a href=""> <i class="fab fa-cc-amazon-pay"></i> การชำระเงิน</a></li>
            <li class="nav-item"><a href="#event"> <i class=" fas fa-search"></i> ตรวจสอบรายชื่อ</a></li>
           
             <li><a class="nav-link" href="{{ route('login') }}"><i class=" fas fa-lock"></i> {{ __('ล๊อกอิน') }}</a></li>
        @else
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('index') }}"><i class="fas fa-home"></i> {{ __('หน้าแรก') }}</a>
         </li>

          <li class="nav-item"><a href="{{ route('view_event') }}"><i class="fas fa-list"></i> {{ __('งานอีเวนท์') }}</a></li>

          <li class="nav-item"><a href=""> <i class="fab fa-cc-amazon-pay"></i> {{ __('การชำระเงิน') }}</a></li>

          <li class="nav-item"><a href="{{ route('event') }}"><i class="fas fa-table"></i> {{ __('รายงานการสมัคร') }}</a></li>
          
         <li class="nav-item">         
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> {{ __('ออกจากระบบ') }} </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
                </form>
        </li>
        @endguest 
      </ul>
    </nav>

  </section>                 
</div>

 
<a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="{{asset('layout/scripts/jquery.min.js')}}"></script>
<script src="{{asset('layout/scripts/jquery.backtotop.js')}}"></script>
<script src="{{asset('layout/scripts/jquery.mobilemenu.js')}}"></script>
</body>
</html>

<main class="py-2">
        <script src="{{asset('core/assets/js/form-elements.js', env('REDIRECT_HTTPS'))}}"></script>
        <script src="{{asset('core/assets/js/form-validation.js', env('REDIRECT_HTTPS'))}}"></script>
        <script src="{{asset('core/vendor/bootstrap-fileinput/jasny-bootstrap.js', env('REDIRECT_HTTPS'))}}"></script>
        @yield('content')
</main>