<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_db', function (Blueprint $table) {
            $table->increments('id');
            $table->increments('users_id');
            $table->string('name_th');
            $table->string('lastname_th');
            $table->string('day');
            $table->string('month');
            $table->string('year');
            $table->string('blood');
            $table->string('address');
            $table->string('telephone');
            $table->string('Idcard');
            $table->string('passport');
            $table->string('country');
            // $table->rememberToken();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('users_db');
    }
}
