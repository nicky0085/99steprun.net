<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EvtEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('evt_event', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_event');
            $table->enum('invitee_type', ['User', 'Faculty', 'Major', 'Course'])->default('Course');
            $table->tinyInteger('is_cms_visible')->default('1');
            $table->tinyInteger('is_api_visible')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
