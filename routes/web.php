<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


	
Route::group(['prefix' => 'Home'], function() {

	Route::get('home', 'EventRegController@home')->name('home');
});

Route::group(['prefix' => 'Event'], function() {

	Route::get('list', 'EventRegController@list')->name('list');	
	Route::get('detail_reg/{id}', 'EventRegController@detail_reg')->where('id', '[0-9]+')->name('detail_reg');
	Route::get('slip_invoice', 'EventRegController@slip_invoice')->name('slip_invoice');
	Route::get('check_name', 'EventRegController@check_name')->name('check_name');

	Route::get('view', 'EventRegController@view')->name('view');	
	Route::post('add_reg', 'EventRegController@add_reg')->name('add_reg');
	Route::post('accept', 'EventRegController@accept')->name('accept');		
});


Route::group(['prefix' => 'Event_admin'], function() {


	Route::post('/save/', 'EventController@save')->name('save');
	Route::post('/generate_code/', 'EventController@generate_code')->name('generate_code');
	Route::get('/vip_event/{id}', 'EventController@vip_event')->where('id', '[0-9]+')->name('vip_event');
	Route::get('/detail_event/{id}', 'EventController@detail_event')->where('id', '[0-9]+')->name('detail_event');
	Route::get('/view_event', 'EventController@view_event')->name('view_event');	

	Route::get('/add_event', 'EventController@add_event')->name('add_event');
		
	Route::get('/report_event', 'EventController@report_event')->name('report_event');	

	Route::get('report_register/{id}', 'EventController@report_register')->where('id', '[0-9]+')->name('report_register');

	Route::get('add_register/{id}', 'EventController@add_register')->where('id', '[0-9]+')->name('add_register');

	Route::get('delete_register/{id}', 'EventController@delete_register')->where('id', '[0-9]+')->name('report_register');

});






Route::group(['prefix' => 'home'], function() {
	Route::get('', 'HomeController@index')->name('index');
	Route::get('index', 'HomeController@index')->name('index');
	Route::get('event', 'HomeController@event')->name('event');
	Route::get('event_list', 'HomeController@event_list')->name('event_list');	
});	



// Route::get('/edit', 'HomeController@edit')->name('edit');
// Route::get('/event_user', 'HomeController@event_user')->name('event_user');


Auth::routes();