<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_vipModel extends Model
{
    protected $fillable = [
        'id',
        'event_id',
        'gencode',
        'name',
        'lastname'
        
      
    ];

    protected $table = "event_vip";
}
