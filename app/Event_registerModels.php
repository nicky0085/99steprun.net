<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_registerModels extends Model
{
     protected $fillable = [
        'id',
        'event_id',
        'name',
        'lastname',
        'telephone',
        'birthday',
        'team',
        'idcard_passport',
        'address',
        'email',
        'address',
        'evt_type',
        'gender',
        'age',
        'size_shirts'
      
    ];

    protected $table = "event_register";
}
