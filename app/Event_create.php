<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_create extends Model
{
   protected $fillable = [
        'id',
        'name_evt',
        'opentdate',
        'limit',
        'description',
        'file',
        'group_id',
        'event_type',
        'price',
        'gender',
        'age',
        'shirts',
        'created_at'
    ];

    protected $table = "event_create";
}
