<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_db extends Model
{
     protected $fillable = [
        'id',
        'users_id',
        'name_th',
        'lastname_th',
        'day',
        'month',
        'year',
        'blood',
        'address',
        'telephone',
        'Idcard',
        'passport',
        'country'
    ];

    protected $table = "users_db";
}
