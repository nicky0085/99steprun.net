<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event_group1 extends Model
{
    protected $fillable = [
        'id',
        'event_id',
        'group_id',
        'event_type',
        'price',
        'age1',
        'age2',
        'age3',
        'age4',
        'age5',
        'age6',
        'age7',
        'age8',
        'age9',
        'age10',
        'age11',
        'age12',
        'age13',
        'age14',
        'age15',
        'age16',
        'shirts'
    ];

    protected $table = "event_group1";
}
