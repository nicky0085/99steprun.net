<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class password_resets extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

}
