<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventModel extends Model
{
   use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    protected $table = "users";

}
