<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventaceptModel extends Model
{
     protected $fillable = [
        'id',
        'user_id',
        'detail',
        'accept'
   
      
    ];

    protected $table = "event_accept";
}
