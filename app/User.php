<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'name_th',
        'lastname_th',
        'day',
        'month',
        'year',
        'blood',
        'address',
        'telephone',
        'Idcard',
        'passport',
        'country'
    ];

    /**
     * The attributes that s,
    'day',
    'month',
    '',
    '',hould be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = "users";
}
