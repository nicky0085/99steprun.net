<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use App\User_db;
use App\User;



class UserController extends Controller
{

	public function view(Request $request )
  	{		

		$this->_data['user'] =  User_db::all();
		
  		return view('user.view')->with($this->_data);
  		
  	}
    
	public function user(Request $request )
	{	
	  		$user = new User_db;
	  		$user->users_id = $request->id;
	  		$user->name_th = $request->name_th;
	  		$user->lastname_th = $request->lastname;
	  		$user->date = $request->day;
	  		$user->month = $request->month;
	  		$user->year = $request->year;
	  		$user->blood = $request->bloodtype;
	  		$user->address = $request->address;
	  		$user->telephone = $request->telephone;
			$user->Idcard = $request->Idcard;
	  		$user->passport = $request->passport;
	  		$user->country = $request->country;
	  		$user->save();

	  		return view('user.view');

	}
	public function edit (Request $request )
	{	
	 return view('user.edit');
	  		
	}

}
