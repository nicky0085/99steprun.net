<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Event_registerModels;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response


     */
    /* ********* Menu Bar **********************/

    public function index()
    { 
       
        return view('admin.list');
    }

    public function event ()
    {    
       return redirect()->route('report_event'); 
     
    }

    public function event_list ()
    {    
        
       return redirect()->route('view_event'); 
     
    }


    public function event_view()
    {

        $this->_data['event_c'] = Event_create::all();


        return view('home')->with($this->_data);
    }
   


}
