<?php
namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;

use App\Event_create;
use App\Event_registerModels;
use App\Event_vipModel;
use App\Event_type;
use App\event_group1;
use App\event_group2;
use App\event_group3;
use App\event_group4;
class EventController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response

     */

    public function view_event()
    {
        $this->_data['event_m'] = Event_create::all();
        return view('admin.event_manage.list')->with($this->_data);
    }
  

    public function add_event(){
        return view('admin.event_manage.add');
    }

    public function save(Request $request){

        $date = date("Y-m-d H:i:s");

        $event = new Event_create;
        $event->name_evt = $request->name_evt;
        $event->opentdate = $request->runtime;
        $event->limit = $request->limit;
        $event->description = $request->detail;
        $event->file = $request->file;
        $event->created_at =  $date;
        $event->save();

        $event_d = Event_create::where('created_at',$date)->get();

        foreach ($event_d  as $key => $value) {

            $id = $value->id ;

            $event_group1 = new event_group1;
            $event_group1->event_id = $id;
            $event_group1->group_id = $request->group_id1;
            $event_group1->event_type = $request->event_type1;
            $event_group1->price = $request->price1;
            $event_group1->gender = $request->gender1;

            $event_group1->age1 =    $request->age1;
            $event_group1->age2 =    $request->age2;
            $event_group1->age3 =    $request->age3;
            $event_group1->age4 =    $request->age4;
            $event_group1->age5 =    $request->age5;
            $event_group1->age6 =    $request->age6;
            $event_group1->age7 =    $request->age7;
            $event_group1->age8 =    $request->age8;
            $event_group1->age9 =    $request->age9;
            $event_group1->age10 =    $request->age10;
            $event_group1->age11 =    $request->age11;
            $event_group1->age12 =    $request->age12;
            $event_group1->age13 =    $request->age13;
            $event_group1->age14 =    $request->age14;
            $event_group1->age15 =    $request->age15;
            $event_group1->age16 =    $request->age16;
          
            $event_group1->shirts = $request->shirts1;
            $event_group1->save();

            $event_group2 = new event_group2;
            $event_group2->event_id = $id;
            $event_group2->group_id = $request->group_id2;
            $event_group2->event_type = $request->event_type2;
            $event_group2->price = $request->price2;
            $event_group2->gender = $request->gender2;

            $event_group2->age1 =    $request->age12;
            $event_group2->age2 =    $request->age22;
            $event_group2->age3 =    $request->age32;
            $event_group2->age4 =    $request->age42;
            $event_group2->age5 =    $request->age52;
            $event_group2->age6 =    $request->age62;
            $event_group2->age7 =    $request->age72;
            $event_group2->age8 =    $request->age82;
            $event_group2->age9 =    $request->age92;
            $event_group2->age10 =    $request->age102;
            $event_group2->age11 =    $request->age112;
            $event_group2->age12 =    $request->age122;
            $event_group2->age13 =    $request->age132;
            $event_group2->age14 =    $request->age142;
            $event_group2->age15 =    $request->age152;
            $event_group2->age16 =    $request->age162;
          
            $event_group2->shirts = $request->shirts2;

            $event_group2->save();

            $event_group3 = new event_group3;
            $event_group3->event_id = $id;
            $event_group3->group_id = $request->group_id3;
            $event_group3->event_type = $request->event_type3;
            $event_group3->price = $request->price3;
            $event_group3->gender = $request->gender3;

            $event_group3->age1 =    $request->age13;
            $event_group3->age2 =    $request->age23;
            $event_group3->age3 =    $request->age33;
            $event_group3->age4 =    $request->age43;
            $event_group3->age5 =    $request->age53;
            $event_group3->age6 =    $request->age63;
            $event_group3->age7 =    $request->age73;
            $event_group3->age8 =    $request->age83;
            $event_group3->age9 =    $request->age93;
            $event_group3->age10 =    $request->age103;
            $event_group3->age11 =    $request->age113;
            $event_group3->age12 =    $request->age123;
            $event_group3->age13 =    $request->age133;
            $event_group3->age14 =    $request->age143;
            $event_group3->age15 =    $request->age153;
            $event_group3->age16 =    $request->age163;
          
            $event_group3->shirts = $request->shirts3;

            $event_group3->save();

            return redirect()->route('view_event'); 
        }
        
        // $this->validate($request, [
        //   'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        // ]);
        }
        
    public function detail_event(Request $Request,$id){

      $this->_data['event_c'] = Event_create::where('id',$id)->get();
      $this->_data['event_g1'] = event_group1::where('event_id',$id)->get();
      $this->_data['event_g2'] = event_group2::where('event_id',$id)->get();
      $this->_data['event_g3'] = event_group3::where('event_id',$id)->get();

       return view('admin.event_manage.detail')->with($this->_data);
    }
    

    public function vip_event(Request $Request ,$id)
    {
       $this->_data['event_m'] = Event_create::where('id',$id)->get();

       $this->_data['vip'] = Event_vipModel::where('event_id',$id)->get();

        return view('admin.event_manage.vip')->with($this->_data);
    }


    public function generate_code(Request $request)
    {
        $id = $request->id;
        $num = $request->number;    

        for ($i = 0; $i < $num; $i++) 
        { 
            $rand = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ23456789'),0,6);
            $vip = new Event_vipModel;
            $vip->event_id =  $id ;
            $vip->gencode =  $rand ;
            $vip->save();
        }       
        return redirect()->route('vip_event',$id);    
    } 

    
    public function report_event()
    {
        $this->_data['event'] = Event_create::all();
        return view('admin.event.report_event')->with($this->_data);
    }


    public function report_register(Request $Request ,$id)
    {

        $this->_data['event'] = Event_create::where('id',$id)->get();
        $this->_data['register'] =  Event_registerModels::where('event_id',$id)->get();
        $register =  Event_registerModels::where('event_id',$id);
        return view('admin.event.report_register')->with($this->_data);
    }


    public function add_register(Request $Request, $id){

      $this->_data['event_c'] = Event_create::where('id',$id)->get();
      $this->_data['event_g1'] = event_group1::where('event_id',$id)->get();
      $this->_data['event_g2'] = event_group2::where('event_id',$id)->get();
      $this->_data['event_g3'] = event_group3::where('event_id',$id)->get();

      

       return view('admin.event.add')->with($this->_data);
    }


    public function delete_register(Request $Request ,$id)
    { 
        $register = Event_registerModels::find('id',$id);
        $register->delete();
        $this->flash_messages($request, 'success', 'Success!');
        return redirect()->route('Event_admin/report_register'); 

    }


}
