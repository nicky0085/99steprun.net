<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event_create;
use App\Event_registerModels;
use App\Event_vipModel;
use App\Event_type;
use App\event_group1;
use App\event_group2;
use App\event_group3;
use App\event_group4;

class EventRegController extends Controller
{
    
    public function home (){
        return view('home');
    }

    public function list (Request $request){

        $this->_data['event'] =  Event_create::all();
        return view('event.list')->with($this->_data); 
    }

    public function slip_invoice(){
        return view('event.slip');

    }

    public function check_name(){
         return view('event.check');
    }

	public function view (Request $request){

		$id="2";
        $this->_data['event'] =  Event_create::where('id',$id)->get();
        return view('event.list')->with($this->_data); 
	}



    public function detail_reg (Request $request ,$id){
    
        $this->_data['event_c'] = Event_create::where('id',$id)->get();
          $this->_data['event_g1'] = event_group1::where('event_id',$id)->get();
          $this->_data['event_g2'] = event_group2::where('event_id',$id)->get();
          $this->_data['event_g3'] = event_group3::where('event_id',$id)->get();

       return view('event.add')->with($this->_data);

    }
    public function add_reg (Request $request){
        

    	$reg_evt = new Event_registerModels;
   
    	$id="2";
    	$reg_evt->event_id = $id;

        $reg_evt->name = $request->firstname;
        $reg_evt->lastname = $request->lastname;
        $reg_evt->telephone = $request->phone;

       	$reg_evt->birthday = $request->birthdate;
        $reg_evt->team = $request->team;
        $reg_evt->idcard_passport = $request->idcard;
        $reg_evt->email = $request->email;
        $reg_evt->address = $request->address;
        
        $reg_evt->evt_type = $request->evt_type;
        $reg_evt->gender = $request->gender;
        $reg_evt->age = $request->age;

        $reg_evt->size_shirts = $request->t_size_id;

        $reg_evt->save();

        return view('event.accept');    
    }



    public function accept(Request $request)
    {

    	$accept = new EventaceptModel;

    	$accept->accept = $request->accept;

    	$accept->save();

    	return view('event.slip'); 
    }


}
