<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_db;
use App\User;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response

     */
    public function Admin()
    {
       
        $this->_data['user'] =  User::all();
        return view('admin.list')->with($this->_data);
    }

    public function check_admin()
    {
    	echo "welcome";
    //return view('admin.list');
    }

    public function view_admin(Request $request ,$id)
    {       

        $this->_data['user_db'] =  User_db::where('users_id',$id)->get();
        $users_db =  User_db::where('users_id',$id);

        return view('admin.view_user')->with($this->_data);
        
    }
}
