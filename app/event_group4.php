<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event_group4 extends Model
{
     protected $fillable = [
        'id',
        'event_id',
        'group_id',
        'event_type',
        'price',
        'age',
        'shirts'
    ];

    protected $table = "event_group4";
}
